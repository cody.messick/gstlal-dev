#
# spec file for package python-pyFFTW
#
# Copyright (c) 2020 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


Name:           python-pyFFTW
Version:        0.11.1
Release:        0
Summary:        A pythonic wrapper around FFTW, the FFT library
License:        GPL-2.0-or-later AND BSD-3-Clause
Group:          Development/Languages/Python
URL:            https://github.com/pyFFTW/pyFFTW
Source:         https://github.com/pyFFTW/pyFFTW/archive/v%{version}.tar.gz
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python%{python3_pkgversion}-Cython
BuildRequires:  python%{python3_pkgversion}-numpy >= 1.6
BuildRequires:  python%{python3_pkgversion}-scipy >= 0.14.0
BuildRequires:  fdupes
BuildRequires:  fftw3-devel
BuildRequires:  python-rpm-macros
Requires:       python%{python3_pkgversion}-numpy >= 1.6
Requires:       python%{python3_pkgversion}-scipy >= 0.14.0

%global common_description %{expand:
pyFFTW is a pythonic wrapper around the FFTW libary.
An interface for all the possible transforms that FFTW can perform is provided.

Both the complex DFT and the real DFT are supported, as well as arbitrary
axes of abitrary shaped and strided arrays, which makes it almost
feature equivalent to standard and real FFT functions of ``numpy.fft`` 
(indeed, it supports the ``clongdouble`` dtype which ``numpy.fft`` does not).

Operating FFTW in multithreaded mode is supported.

A comprehensive unittest suite can be found with the source on the github 
repository.}

%description
%common_description

%package -n python3-pyFFTW
Summary:    %{summary}

%description -n python3-pyFFTW
%common_description

%prep
%setup -q -n pyFFTW-%{version}

%build
export CFLAGS="%{optflags}"
%py3_build

%install
%py3_install
%{python3} -m compileall -d %{python3_sitelib} %{buildroot}%{python3_sitelib}/pyfftw/
%{python3} -O -m compileall -d %{python3_sitelib} %{buildroot}%{python3_sitelib}/pyfftw/
%fdupes %{buildroot}%{python3_sitelib}

%check
%{python3} setup.py build_ext --inplace
# disable tests for now, they take a very long time
#%{python3} setup.py test
%{python3} setup.py clean

%files -n python3-pyFFTW
#%doc README.md
%license LICENSE.txt
%{python3_sitearch}/pyfftw/
%{python3_sitearch}/pyFFTW-%{version}-py%{python3_version}.egg-info

%changelog
* Sun Feb 21 2021 Patrick Godwin <patrick.godwin@ligo.org> 0.11.1-0
- Updates for CentOS 7 build
