# This is an attempt to revamp the optimized gstlal development 
# container. The base is the lalsuite-development container. 

FROM containers.ligo.org/alexander.pace/mkl-dev/mkl-dev:el7

# Labeling/packaging stuff:
LABEL name="GstLAL Development Package, EL7" \
      maintainer="Alexander Pace <alexander.pace@ligo.org>" \
      date="2021-03-11" \
      support="Reference Platform, EL7"

USER root

## Add lscsoft-backports-testing repo. This is a temporary
## step before packages go into production.

RUN sed 's/production/backports-testing/g' /etc/yum.repos.d/lscsoft-production.repo > /etc/yum.repos.d/lscsoft-backports-testing.repo

## Copy Optimized RPMs to container
COPY rpms /rpms

# Install Optimized RPMs, delete old RPMs
RUN yum makecache && \
      yum -y localinstall /rpms/*.rpm 

# Copy spec file patching script:
RUN chmod +x /rpms/patch_optimized_spec_file
RUN cp /rpms/patch_optimized_spec_file /usr/bin/

# Install lalsuite packages from source:
RUN mkdir /src && \
    cd /src && \
    git clone https://git.ligo.org/lscsoft/lalsuite.git && \
    cd /src/lalsuite && \
    for app in "lal" "lalframe" "lalmetaio" "lalsimulation" "lalburst" "lalinspiral" "lalpulsar" "lalinference" "lalapps"; do \
        cd $app; \
        ./00boot; \
        /usr/bin/patch_optimized_spec_file -c gcc -k --nocheck -f ./$app.spec.in; \
        ./configure --enable-swig; \
        make dist; \
        yum-builddep -y $app.spec; \
        rpmbuild -tb ./*.tar.xz; \
        yum -y install /root/rpmbuild/RPMS/x86_64/*.rpm; \
        cd ..; \
    done && \
    yum clean all && \
    rm -rf /src /root

## Install development components for gstlal.

# Install gstreamer and associated packages. I yum info'd gstreamer1, and 
# it was installing from lscsoft-backports. So I think it should be 
# good to go. 

RUN yum -y install gstreamer1 \
                   gstreamer1-devel \
                   gstreamer1-plugins-base-devel \
                   gstreamer1-plugins-good \
                   gstreamer1-plugins-base-devel \
                   python3-gstreamer1 \
                   gstreamer1-plugins-bad-free \
                   gobject-introspection \
                   gobject-introspection-devel \
                   glib2-devel \
                   pygobject3-devel && \
    rm -rf /var/cache/yum

# Install GDS for online

RUN yum -y install gds-lsmp \
                   gds-lsmp-devel \
                   gds-framexmit \
                   gds-framexmit-devel && \
    rm -rf /var/cache/yum
           
# Try installing gstlal-ugly dependencies
RUN yum -y install ldas-tools-framecpp \
                   ldas-tools-framecpp-c \
                   ldas-tools-framecpp-c-devel \
                   ldas-tools-framecpp-devel \
                   ldas-tools-framecpp-doc \
                   ldas-tools-framecpp-swig \
                   nds2-client-devel \
                   nds2-client-headers && \
    rm -rf /var/cache/yum

# Install gstlal dependendies:

RUN yum -y install gtk-doc \
                   graphviz && \
    rm -rf /var/cache/yum

RUN yum -y install python36-ligo-lvalert \
                   python36-ligo-lw \
                   python36-confluent-kafka \
                   python36-jinja2 \
                   python36-pandas && \
    rm -rf /var/cache/yum

# Install minimal latex dependencies (plots):
RUN yum -y install texlive-latex-bin \
                   texlive-collection-fontsrecommended \
                   texlive-type1cm && \
    rm -rf /var/cache/yum

# Install avahi-daemon
RUN yum -y install avahi \
                   avahi-libs \
                   avahi-ui-gtk3 \
                   avahi-ui-tools \
                   avahi-autoipd && \
    rm -rf /var/cache/yum

# Install support packages
RUN yum -y install dqsegdb \
                   vim && \
    rm -rf /var/cache/yum

# Install HTCondor python bindings
# NOTE: remove dev repo when needed features are in stable release series
RUN cd /etc/yum.repos.d && \
    wget http://research.cs.wisc.edu/htcondor/yum/repo.d/htcondor-development-rhel7.repo && \
    wget http://research.cs.wisc.edu/htcondor/yum/repo.d/htcondor-stable-rhel7.repo
RUN wget http://research.cs.wisc.edu/htcondor/yum/RPM-GPG-KEY-HTCondor && \
    rpm --import RPM-GPG-KEY-HTCondor && \
    rm -rf RPM-GPG-KEY-HTCondor
RUN yum install -y python3-condor && \
    rm -rf /var/cache/yum

# I think these changing these avahi daemon flags accomplish two
# goals:

# This allows for multiple instances of avahi *with the same PID*
# to run on the same machine simultaneously. This necessary for when
# different CI pipelines run on at the same time on the gitlab runner 
# machine. I could also see this being an issue if multiple containers 
# get spun up on one computer. You don't see this issue run running 
# multiple instances of gstlal on one machine because there's only one
# avahi-daemon running:

RUN sed -i 's/rlimit-nproc=3/rlimit-nproc=333/g' /etc/avahi/avahi-daemon.conf

# This allows for avahi to run without dbus. I think this was a security
# thing for CentOS containers.

RUN sed -i 's/#enable-dbus=yes/enable-dbus=no/g' /etc/avahi/avahi-daemon.conf

# Clean up and close-out

RUN rm -rf /tars /src /rpms && \
    rm -rf /var/cache/yum && \
    yum clean all

# Build gstreamer registry cache:

# RUN gst-inspect-1.0

# Export MKL environment variables: 

ENV MKL_INTERFACE_LAYER LP64
ENV MKL_THREADING_LAYER SEQUENTIAL


ENTRYPOINT nohup bash -c "avahi-daemon --no-rlimits &"  && \
           bash
